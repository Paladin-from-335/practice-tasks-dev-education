package com.github.arr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class ArraysTasksFrom1To7 {
    public static void main(String[] args) {
        // Task 1
        System.out.println("Array of even numbers: " + Arrays.toString(arrayEven(2,100)));

        // Task 2
        System.out.println("Array of odd numbers: " + Arrays.toString(arrayOdd(1,99)));

        // Task 3
        System.out.println("Array of random numbers in a range: " + Arrays.toString(severalRandomNumInRange(15,0, 9)));

        // Task 4
        System.out.println("Array of random numbers in a range: " + Arrays.toString(severalRandomNumInRange(8,0, 8)));

        // Task 5
        System.out.println("Array of random numbers in a range: " + Arrays.toString(severalRandomNumInRange(4,10, 99)));

        // Task 6
        System.out.println("The Fibonacci sequence is " + Arrays.toString(arrayFibonacci(20)));

        // Task 7
        System.out.println("The bigest number in random array is " + maxElementInArray(severalRandomNumInRange(12,-15,15)));

    }

    public static Object[] arrayEven(int first, int last){
        ArrayList<Integer> array = new ArrayList<>();
        for(int i = first; i < last+1; i++){
            if(i%2 == 0){
                array.add(i);
            }
        }
        return array.toArray();
    }

    public static Object[] arrayOdd(int first, int last){
        ArrayList<Integer> array = new ArrayList<>();
        for(int i = first; i < last+1; i++){
            if(i%2 != 0){
                array.add(i);
            }
        }
        return array.toArray();
    }

    public static int[] severalRandomNumInRange(int size, int min, int max){
        int[] sevRanNum = new int[size];
        Random random = new Random();
        for(int i = 0; i < size; i++){
            sevRanNum[i] = random.nextInt(max - min +1 ) + min;
        }
        return sevRanNum;
    }

    public static int[] arrayFibonacci(int size){
        int[] fibonacci = new int[size];
        fibonacci[0] = 0;
        fibonacci[1] = 1;

        for(int i = 2; i < size; i++){
            fibonacci[i] = fibonacci[i-2] + fibonacci[i-1];
        }
        return fibonacci;
    }

    public static int maxElementInArray(int[] array){
        int max = array[0];
        for(int i = 1; i < array.length; i++){
            if(max < array[i]){
                max = array[i];
            }
        }
        return max;
    }
}
