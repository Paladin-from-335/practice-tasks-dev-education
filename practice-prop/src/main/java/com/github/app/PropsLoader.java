package com.github.app;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PropsLoader {

    private static final Map<String, String> properties = new HashMap<>();

    public static String getProperties(String path, String props) throws IOException {
        FileReader fr = new FileReader(path);
        BufferedReader br = new BufferedReader(fr);
        String str;

        while ((str = br.readLine()) != null) {
            String [] tmp = str.split("=");
            properties.put(tmp[0].trim(), tmp[1].trim());
        }
        if (properties.get(props) !=null) {
            return properties.get(props);
        } else {
            throw new NullPointerException("Wrong key");
        }
    }
}
