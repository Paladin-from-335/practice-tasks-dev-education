package com.github;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

public class SimpleCustomHandler implements HttpHandler {


    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String testResponse = "Test";
        exchange.sendResponseHeaders(200,testResponse.length());
        OutputStream os = exchange.getResponseBody();
        os.write(testResponse.getBytes());
        os.close();
    }
}
