package com.github;

import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;

public class SimpleServerSocket {

    public static void engSocket() {
        int port = 8081;
        try {
            ServerSocket serverSocket = new java.net.ServerSocket(port);
            System.out.println("Server started with port " + port);

            while (true) {

                Socket socket = serverSocket.accept();
                InputStream in = socket.getInputStream();
                OutputStream out = socket.getOutputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));

                String str;

                while ((str = br.readLine()) != null && !str.isEmpty()) {
                    System.out.println(str);
                }

                String outString = "Olololo";

                bw.write("HTTP/1.1 200 OK\n\r" +
                        "Content-Length: " + outString.length() + "\n\r" +
                        "Content-Type: text/plain\n\r"
                );


                bw.write("\n\r" + outString + "\n\r\n\r");

                System.out.println("Socket closed.");

                bw.flush();
                bw.close();
                br.close();
                socket.close();

            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
