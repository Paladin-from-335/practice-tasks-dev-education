package com.gitgub.student;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class DaoStudentTest {
    @Test
    public void facultyFinder(){
        Student s1 = new Student(1, "A", "A", "A", 1990, "Q", "001", "one", "1", 1);
        Student s2 = new Student(2, "B", "B", "B", 1991, "Q", "002", "one", "2", 2);
        Student s3 = new Student(3, "C", "C", "C", 1991, "L", "003", "two", "1", 1);
        Student s4 = new Student(4, "D", "D", "D", 1993, "L", "004", "two", "2", 2);
        Student s5 = new Student(5, "E", "E", "E", 1994, "K", "005", "one", "1", 1);
        Student s6 = new Student(6, "F", "F", "F", 1994, "N", "006", "three", "1", 3);
        ArrayList<Student> st = new ArrayList<>();
        st.add(s1);
        st.add(s2);
        st.add(s3);
        st.add(s4);
        st.add(s5);
        st.add(s6);

        ArrayList<Student> exp = new ArrayList<>();
        exp.add(s1);
        exp.add(s2);
        exp.add(s5);

        List<Student> act = DaoStudent.facultyFinder(st, "one");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void allStudentOnFaculty(){

        Student s1 = new Student(1, "A", "A", "A", 1990, "Q", "001", "one", "1", 1);
        Student s2 = new Student(2, "B", "B", "B", 1991, "Q", "002", "one", "2", 2);
        Student s3 = new Student(3, "C", "C", "C", 1991, "L", "003", "two", "1", 1);
        Student s4 = new Student(4, "D", "D", "D", 1993, "L", "004", "two", "2", 2);
        Student s5 = new Student(5, "E", "E", "E", 1994, "K", "005", "one", "1", 1);
        Student s6 = new Student(6, "F", "F", "F", 1994, "N", "006", "three", "1", 3);
        ArrayList<Student> st = new ArrayList<>();
        st.add(s1);
        st.add(s2);
        st.add(s3);
        st.add(s4);
        st.add(s5);
        st.add(s6);

        ArrayList<Student> stud11 = new ArrayList<>();
        stud11.add(s1);
        stud11.add(s5);
        ArrayList<Student> stud12 = new ArrayList<>();
        stud12.add(s3);
        ArrayList<Student> stud13 = new ArrayList<>();
        stud13.add(s6);
        ArrayList<Student> stud21 = new ArrayList<>();
        stud21.add(s2);
        ArrayList<Student> stud22 = new ArrayList<>();
        stud22.add(s4);

        HashMap<String, ArrayList<Student>> faculties1 = new HashMap<>();
        faculties1.put("one", stud11);
        faculties1.put("two", stud12);
        faculties1.put("three", stud13);

        HashMap<String, ArrayList<Student>> faculties2 = new HashMap<>();
        faculties2.put("one", stud21);
        faculties2.put("two", stud22);

        HashMap<String, HashMap<String, ArrayList<Student>>> course = new HashMap<>();
        course.put("1", faculties1);
        course.put("2", faculties2);

        HashMap<String, HashMap<String, ArrayList<Student>>>  act = DaoStudent.allStudentOnFaculty(st);
        Assert.assertEquals(course, act);

    }

}