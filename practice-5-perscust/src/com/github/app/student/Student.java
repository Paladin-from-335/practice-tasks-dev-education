package com.gitgub.student;

import java.util.Objects;

public class Student {
    long id;
    String surname;
    String firstName;
    String patronymic;
    int birthdayYear;
    String address;
    String phone;
    String faculty;
    String course;
    int group;

    public Student() {
    }

    public Student(long id, String surname, String firstName, String patronymic, int birthdayYear, String address, String phone, String faculty, String course, int
            group) {
        this.id = id;
        this.surname = surname;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.birthdayYear = birthdayYear;
        this.address = address;
        this.phone = phone;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", firstName='" + firstName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", birthdayYear=" + birthdayYear +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", faculty='" + faculty + '\'' +
                ", course='" + course + '\'' +
                ", group='" + group + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id && birthdayYear == student.birthdayYear && Objects.equals(surname, student.surname) && Objects.equals(firstName, student.firstName) && Objects.equals(patronymic, student.patronymic) && Objects.equals(address, student.address) && Objects.equals(phone, student.phone) && Objects.equals(faculty, student.faculty) && Objects.equals(course, student.course) && Objects.equals(group, student.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, surname, firstName, patronymic, birthdayYear, address, phone, faculty, course, group);
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setBirthdayYear(int birthdayYear) {
        this.birthdayYear = birthdayYear;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public long getId() {
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public int getBirthdayYear() {
        return birthdayYear;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getCourse() {
        return course;
    }

    public int getGroup() {
        return group;
    }
}