package com.github.customer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Customer {
    long id;
    String surname;
    String firstName;
    String patronymic;
    String address;

    public long getId() {
        return id;
    }

    public int getNumberCreditCard() {
        return numberCreditCard;
    }

    int numberCreditCard;
    int bankAccount;

    List<Object> customers = new ArrayList<>();

    public Customer(long id, String surname, String firstName, String patronymic, String address, int numberCreditCard, int bankAccount) {
        this.id = id;
        this.surname = surname;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.address = address;
        this.numberCreditCard = numberCreditCard;
        this.bankAccount = bankAccount;
    }

    public static void main(String[] args) {

    }

    public static Customer getCustomerByCreditCard(ArrayList<Customer> customers, int findNumberCreditCard) {
        Customer findCustomer = null;
        for (Customer c : customers) {
            if (c.getNumberCreditCard() == findNumberCreditCard) {
                findCustomer = c;
                break;
            }
        }
        return findCustomer;
    }

    public static List<Customer> duplicatesByName(ArrayList<Customer> customers) {
        List<Customer> duplicatesByName = new ArrayList<>();
        Set<Customer> set = new HashSet<>();
        for (Customer c : customers) {
            if (set.contains(c)) {
                duplicatesByName.add(c);
            } else {
                set.add(c);
            }
        }
        return duplicatesByName;
    }
}
