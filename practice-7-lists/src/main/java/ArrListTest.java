import org.junit.Assert;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class ArrListTest {

    private IList list = new ArrList();

    //=================================================
    //=================== Clean ========================
    //=================================================

    @Test
    public void clearMany() {
        list = new ArrList(MockData.arrTen);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearTwo() {
        list = new ArrList(MockData.arrTwo);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearOne() {
        list = new ArrList(MockData.arrOne);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearZero() {
        list = new ArrList(MockData.arrZero);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void clearNull() {
        list = new ArrList(MockData.arrNull);
        this.list.clear();
    }

    //=================================================
    //=================== Size ========================
    //=================================================

    @Test
    public void sizeMany() {
        list = new ArrList(MockData.arrTen);
        int exp = MockData.arrTen.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeTwo() {
        list = new ArrList(MockData.arrTwo);
        int exp = MockData.arrTen.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeOne() {
        list = new ArrList(MockData.arrOne);
        int exp = MockData.arrTen.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeZero() {
        list = new ArrList(MockData.arrZero);
        int exp = MockData.arrTen.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void sizeNull() {
        list = new ArrList(MockData.arrNull);
        list.size();
    }

    //=================================================
    //=================== Add Start ===================
    //=================================================

    @Test
    public void addStartMany() {
        IList iList = new ArrList(MockData.arrTen);
        iList.addStart(MockData.pushedNum);
        Assert.assertEquals(iList.toArray()[0], MockData.pushedNum);
    }

    @Test
    public void addStartTwo() {
        IList iList = new ArrList(MockData.arrTwo);
        iList.addStart(MockData.pushedNum);
        Assert.assertEquals(iList.toArray()[0], MockData.pushedNum);
    }

    @Test
    public void addStartOne() {
        IList iList = new ArrList(MockData.arrOne);
        iList.addStart(MockData.pushedNum);
        Assert.assertEquals(iList.toArray()[0], MockData.pushedNum);
    }

    @Test
    public void addStartZero() {
        IList iList = new ArrList(MockData.arrZero);
        iList.addStart(MockData.pushedNum);
        Assert.assertEquals(iList.toArray()[0], MockData.pushedNum);
    }

    @Test(expected = NullPointerException.class)
    public void addStartNull() {
        IList iList = new ArrList(MockData.arrNull);
        iList.addStart(MockData.pushedNum);
    }

    //=================================================
    //=================== Add End =====================
    //=================================================

    @Test
    public void addEndMany() {
        IList iList = new ArrList(MockData.arrTen);
        iList.addEnd(MockData.pushedNum);
        Assert.assertEquals(iList.toArray()[0], MockData.pushedNum);
    }

    @Test
    public void addEndTwo() {
        IList iList = new ArrList(MockData.arrTwo);
        iList.addEnd(MockData.pushedNum);
        Assert.assertEquals(iList.toArray()[0], MockData.pushedNum);
    }

    @Test
    public void addEndOne() {
        IList iList = new ArrList(MockData.arrOne);
        iList.addEnd(MockData.pushedNum);
        Assert.assertEquals(iList.toArray()[0], MockData.pushedNum);
    }

    @Test
    public void addEndZero() {
        IList iList = new ArrList(MockData.arrZero);
        iList.addEnd(MockData.pushedNum);
        Assert.assertEquals(iList.toArray()[0], MockData.pushedNum);
    }

    @Test(expected = NullPointerException.class)
    public void addEndNull() {
        IList iList = new ArrList(MockData.arrNull);
        iList.addEnd(MockData.pushedNum);
    }

    //=================================================
    //================ Add By Position ================
    //=================================================

    @Test
    public void addByPosMany() {
        list = new ArrList(MockData.arrTen);
        list.addByPos(5, MockData.pushedNum);
        int[] exp = {0, 1, 2, 33, 44, 555, 999, 6666, 77, 89, 98};
        int[] act = list.toArray();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addByPosTwo() {
        list = new ArrList(MockData.arrTwo);
        list.addByPos(2, MockData.pushedNum);
        int[] exp = {22, 999, 33};
        int[] act = list.toArray();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addByPosOne() {
        list = new ArrList(MockData.arrOne);
        list.addByPos(0, MockData.pushedNum);
        int[] exp = {111, 0};
        int[] act = list.toArray();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addByPosZero() {
        list = new ArrList(MockData.arrZero);
        list.addByPos(0, MockData.pushedNum);
        int[] exp = {111};
        int[] act = list.toArray();
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void addByPosNull() {
        list = new ArrList(MockData.arrNull);
        list.addByPos(0, MockData.pushedNum);
    }

    //=================================================
    //================ remove start ===================
    //=================================================

    @Test
    public void removeStartMany() {
        list = new ArrList(MockData.arrTen);
        list.removeStart();
        int[] exp = MockData.arrRemoveStartTen;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeStartTwo() {
        list = new ArrList(MockData.arrTwo);
        list.removeStart();
        int[] exp = MockData.arrRemoveStartTwo;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeStartOne() {
        list = new ArrList(MockData.arrOne);
        list.removeStart();
        int[] exp = MockData.arrRemoveStartOne;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeStartZero() {
        list = new ArrList(MockData.arrZero);
        list.removeStart();
    }

    @Test(expected = NullPointerException.class)
    public void removeStartNull() {
        list = new ArrList(MockData.arrNull);
        list.removeStart();
    }

    //=================================================
    //================ remove end =====================
    //=================================================

    @Test
    public void removeEndMany() {
        list = new ArrList(MockData.arrTen);
        list.removeEnd();
        int[] exp = MockData.arrRemoveEndTen;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeEndTwo() {
        list = new ArrList(MockData.arrTwo);
        list.removeEnd();
        int[] exp = MockData.arrRemoveEndTwo;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeEndOne() {
        list = new ArrList(MockData.arrOne);
        list.removeEnd();
        int[] exp = MockData.arrRemoveEndOne;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeEndZero() {
        list = new ArrList(MockData.arrZero);
        list.removeEnd();
    }

    @Test(expected = NullPointerException.class)
    public void removeEndNull() {
        list = new ArrList(MockData.arrNull);
        list.removeEnd();
    }

    //=================================================
    //============== remove by position ===============
    //=================================================

    @Test
    public void removeByPosMany() {
        list = new ArrList(MockData.arrTen);
        list.removeByPos(4);
        int[] exp = {0, 1, 2, 33, 555, 6666, 77, 89, 98};
        int[] act = list.toArray();
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeOutOfBounds() {
        list = new ArrList(MockData.arrTen);
        list.removeByPos(12);

    }

    @Test
    public void removeByPosTwo() {
        list = new ArrList(MockData.arrTwo);
        list.removeByPos(1);
        int[] exp = {22};
        int[] act = list.toArray();
        Assert.assertEquals(exp, act);

    }

    @Test
    public void removeByPosOne() {
        list = new ArrList(MockData.arrTwo);
        list.removeByPos(0);
        int[] exp = {111};
        int[] act = list.toArray();
        Assert.assertEquals(exp, act);

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeByPosZero() {
        list = new ArrList(MockData.arrZero);
        list.removeByPos(0);
    }

    @Test(expected = NullPointerException.class)
    public void removeByPosNull() {
        list = new ArrList(MockData.arrNull);
        list.removeByPos(0);
    }

    //=================================================
    //================= max value =====================
    //=================================================

    @Test
    public void maxNumMany() {
        list = new ArrList(MockData.arrTen);
        int exp = 6666;
        int act = list.max();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maxNumTwo() {
        list = new ArrList(MockData.arrTwo);
        int exp = 33;
        int act = list.max();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maxNumOne() {
        list = new ArrList(MockData.arrOne);
        int exp = 111;
        int act = list.max();
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void maxNumZero() {
        list = new ArrList(MockData.arrZero);
        list.max();
    }

    @Test(expected = NullPointerException.class)
    public void maxNumNull() {
        list = new ArrList(MockData.arrNull);
        list.max();
    }

    //=================================================
    //================= max value =====================
    //=================================================

    @Test
    public void minNumMany() {
        list = new ArrList(MockData.arrTen);
        int exp = 0;
        int act = list.min();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minNumTwo() {
        list = new ArrList(MockData.arrTwo);
        int exp = 22;
        int act = list.min();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minNumOne() {
        list = new ArrList(MockData.arrOne);
        int exp = 111;
        int act = list.min();
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void minNumZero() {
        list = new ArrList(MockData.arrZero);
        list.min();
    }

    @Test(expected = NullPointerException.class)
    public void minNumNull() {
        list = new ArrList(MockData.arrNull);
        list.min();
    }

    //=================================================
    //================= max value =====================
    //=================================================

    @Test
    public void maxPosMany() {
        list = new ArrList(MockData.arrTen);
        int exp = 9;
        int act = list.maxPos();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maxPosTwo() {
        list = new ArrList(MockData.arrTwo);
        int exp = 1;
        int act = list.maxPos();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maxPosOne() {
        list = new ArrList(MockData.arrOne);
        int exp = 0;
        int act = list.maxPos();
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void maxPosZero() {
        list = new ArrList(MockData.arrZero);
        list.maxPos();
    }

    @Test(expected = NullPointerException.class)
    public void maxPosNull() {
        list = new ArrList(MockData.arrNull);
        list.maxPos();
    }

    //=================================================
    //================= min value =====================
    //=================================================

    @Test
    public void minPosMany() {
        list = new ArrList(MockData.arrTen);
        int exp = 0;
        int act = list.minPos();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minPosTwo() {
        list = new ArrList(MockData.arrTwo);
        int exp = 0;
        int act = list.minPos();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minPosOne() {
        list = new ArrList(MockData.arrOne);
        int exp = 0;
        int act = list.minPos();
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void minPosZero() {
        list = new ArrList(MockData.arrZero);
        list.minPos();
    }

    @Test(expected = NullPointerException.class)
    public void minPosNull() {
        list = new ArrList(MockData.arrNull);
        list.minPos();
    }

    //=================================================
    //===================== Sort ======================
    //=================================================

    @Test
    public void sortMany() {
        int[] array = {25, 17, 23, 0, 2, 5, 1};
        list = new ArrList(array);
        int[] exp = {0, 1, 2, 5, 17, 23, 25};
        int[] act = list.sort();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void sortTwo() {
        int[] array = {99, 2};
        list = new ArrList(array);
        int[] exp = {2, 99};
        int[] act = list.sort();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void sortOne() {
        int[] array = {5};
        list = new ArrList(array);
        int[] exp = {5};
        int[] act = list.sort();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void sortZero() {
        list = new ArrList(MockData.arrZero);
        int[] exp = {};
        int[] act = list.sort();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void sortNull() {
        list = new ArrList(MockData.arrNull);
        list.sort();
    }

    //=================================================
    //===================== Get =======================
    //=================================================

    @Test
    public void getMany() {
        list = new ArrList(MockData.arrTen);
        int exp = 33;
        int act = list.get(3);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void getTwo() {
        list = new ArrList(MockData.arrTwo);
        int exp = 33;
        int act = list.get(1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void getOne() {
        list = new ArrList(MockData.arrOne);
        int act = list.get(0);
        int exp = 0;
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getOutOfBounds() {
        list = new ArrList(MockData.arrZero);
        list.get(5);
    }

    @Test(expected = NullPointerException.class)
    public void getNull() {
        list = new ArrList(MockData.arrNull);
        list.get(5);
    }

    //=================================================
    //================== Half revers ==================
    //=================================================

    @Test
    public void halfReversMany() {
        int[] array = {1, 2, 3, 4, 5, 6};
        list = new ArrList(array);
        int[] exp = {4, 5, 6, 0, 1, 2, 3};
        int[] act = list.halfRevers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversTwo() {
        list = new ArrList(MockData.arrTwo);
        int[] exp = {33,22};
        int[] act = list.halfRevers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversOne() {
        list = new ArrList(MockData.arrOne);
        int[] exp = {111};
        int[] act = list.halfRevers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversZero() {
        list = new ArrList(MockData.arrZero);
        int[] exp = {};
        int[] act = list.halfRevers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void halfReverseOddAmount() {
        int[] array = {33, 22, 11};
        list = new ArrList(array);
        int[] exp = {11, 22, 33};
        int[] act = list.halfRevers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void halfReverseNull() {
        list = new ArrList(MockData.arrNull);
        list.halfRevers();
    }

    //=================================================
    //==================== Revers =====================
    //=================================================


    @Test
    public void reversMany() {
        list = new ArrList(MockData.arrTen);
        int[] exp = {98, 89, 77, 6666, 555, 44, 33, 2, 1, 0};
        int[] act = list.revers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reversTwo() {
        list = new ArrList(MockData.arrTwo);
        int[] exp = {33, 22};
        int[] act = list.revers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reversOne() {
        list = new ArrList(MockData.arrOne);
        int[] exp = {111};
        int[] act = list.revers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reversZero() {
        list = new ArrList(MockData.arrZero);
        int[] exp = {};
        int[] act = list.revers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void reversNull() {
        list = new ArrList(MockData.arrNull);
        list.revers();
    }

    //=================================================
    //===================== Set ======================
    //=================================================

    @Test
    public void setMany() {
        list = new ArrList(MockData.arrTen);
        list.set(5, 654);
        int[] exp = {0, 1, 2, 33, 44, 654, 6666, 77, 89, 98};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void setTwo() {
        list = new ArrList(MockData.arrTwo);
        list.set(1, 1111);
        int[] exp = {22, 1111};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void setOne() {
        list = new ArrList(MockData.arrOne);
        list.set(0, 555);
        int[] exp = {555};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setOutOfBounds() {
        list = new ArrList(MockData.arrZero);
        list.set(1, 12);
    }

    @Test(expected = NullPointerException.class)
    public void setNull() {
        list = new ArrList(MockData.arrNull);
        list.set(1, 12);
    }

}

