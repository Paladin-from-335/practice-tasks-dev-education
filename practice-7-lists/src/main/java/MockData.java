public class MockData {

    static int pushedNum = 999;

    static int[] arrTen = {0, 1, 2, 33, 44, 555, 6666, 77, 89, 98};

    static int[] arrTwo = {22, 33};

    static int[] arrOne = {111};

    static int[] arrZero = {};

    static int[] arrNull = null;

    static int[] arrRemoveStartTen = {1, 2, 33, 44, 555, 6666, 77, 89, 98};

    static int[] arrRemoveStartTwo = {33};

    static int[] arrRemoveStartOne = {};

    static int[] arrRemoveEndTen = {0, 1, 2, 33, 44, 555, 6666, 77, 89};

    static int[] arrRemoveEndTwo = {22};

    static int[] arrRemoveEndOne = {};

}
