package com.github.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class InvalidName {

public static String Invalid(String inp){

        Pattern pattern = Pattern.compile("[<>/\\:*?|\"]");
        Matcher matcher = pattern.matcher(inp);
        if(matcher.find()){
            return "Invalid";
        } else {
            return "Valid";
        }
    }
}
