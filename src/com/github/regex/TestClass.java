package com.github.regex;

import org.junit.Assert;
import org.junit.Test;

public class TestClass {
    @Test
    public void TestSpace() {
        String exp = "Aaa! Bbb. Ccc, Ddd; Eee: Ooo? ";
        String act = com.github.regex.Space.Space("Aaa!Bbb.Ccc,Ddd;Eee:Ooo?");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void Direction() {
        String exp = "C\\\\P\\\\J\\\\B";
        String act = com.github.regex.Direction.Direction("C/P/J/B");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void Invalid() {
        String exp = "Invalid";
        String act = com.github.regex.InvalidName.Invalid("<>");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void Valid() {
        String exp = "Valid";
        String act = com.github.regex.InvalidName.Invalid("text");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void DiskDir() {
        boolean exp = true;
        boolean act = com.github.regex.DiskDirect.DiskDir("X:\\P\\ddd\\");
        Assert.assertEquals(exp, act);
    }
        @Test
            public void DiskDirFal() {
                boolean exp = false;
                boolean act = com.github.regex.DiskDirect.DiskDir("c:\\D/s\\");
                Assert.assertEquals(exp, act);
            }
    @Test
    public  void  DirectToFile() {
        boolean exp = true;
        boolean act = com.github.regex.DirectToFile.DirectToFile("d\\");
        Assert.assertEquals(exp, act);
    }
    @Test
    public void NameFromDir(){
        String exp = "TestClass.java";
        String act = com.github.regex.NameFromDir.NameFromDir("src/com/github/regex/TestClass.java");
        Assert.assertEquals(exp, act);
    }
}