package com.github.regex;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DiskDirect {

public static boolean DiskDir(String inp) {
    Pattern patternFalse = Pattern.compile("[/]");
    Matcher matcherFalse = patternFalse.matcher(inp);
    Pattern pattern = Pattern.compile("[a-z]||[A-Z]:\\\\");
    Matcher matcher = pattern.matcher(inp);
    if (matcher.find() && !matcherFalse.find()){
        return true;
    }else if(matcherFalse.find()){
        return false;
    }else {
        return false;
    }

}
}
