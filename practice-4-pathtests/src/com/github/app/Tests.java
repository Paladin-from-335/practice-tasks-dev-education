package com.github.app;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Tests {


    @Test
    public void addSpaceSmall(){
        String exp = ", . : ! ?";
        String act = Tasks.addSpace(",.:!?");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addSpaceNull(){
        String exp = "Error! No text to edit!";
        String act = Tasks.addSpace(null);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addSpaceZero(){
        String exp = "Hello";
        String act = Tasks.addSpace("Hello");
        Assert.assertEquals(exp, act);
    }


    @Test
    public void addSpaceSmall2(){
        String exp = ", . : ! ?";
        String act = Tasks.addSpacePatMat(",.:!?");
        Assert.assertEquals(exp, act);
    }
    @Test
    public void addSpaceNull2(){
        String exp = "Error! No text to edit!";
        String act = Tasks.addSpacePatMat(null);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addSpaceZero2(){
        String exp = "Hello";
        String act = Tasks.addSpacePatMat("Hello");
        Assert.assertEquals(exp, act);
    }


    @Test
    public void replaceSlashSmall(){
        String exp = "D:\\\\home\\\\desktop\\\\task2.java";
        String act = Tasks.replaceSlash("D:/home/desktop/task2.java");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void replaceSlashNull(){
        String exp = "Error! No text to edit!";
        String act = Tasks.replaceSlash(null);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void replaceSlashZero() {
        String exp = "Hello";
        String act = Tasks.replaceSlash("Hello");
        Assert.assertEquals(exp, act);
    }


    @Test
    public void validCharBig(){
        String exp = "The path consists invalids chars.";
        String act = Tasks.validChar("D:/home/desktop/task2.java");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void validCharNull(){
        String exp = "Error! No text to check!";
        String act = Tasks.validChar(null);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void validCharZero() {
        String exp = "The path is valid.";
        String act = Tasks.validChar("Hello");
        Assert.assertEquals(exp, act);
    }


    @Test
    public void checkIsDiskBig(){
        String exp = "Valid path.";
        String act = Tasks.checkIsDisk("D:/home/desktop/task2.java");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void checkIsDiskNull(){
        String exp = "Error! No text to check!";
        String act = Tasks.checkIsDisk(null);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void checkIsDiskZero() {
        String exp = "Not valid path.";
        String act = Tasks.checkIsDisk("Hello");
        Assert.assertEquals(exp, act);
    }

    // Task 5

    @Test
    public void checkIsPathBig(){
        String exp = "Valid path.";
        String act = Tasks.checkIsPath("D:/home/desktop/task2.java/");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void checkIsPathNull(){
        String exp = "Error! No text to check!";
        String act = Tasks.checkIsPath(null);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void checkIsPathZero() {
        String exp = "Not valid path.";
        String act = Tasks.checkIsPath("C//Users\\a.kuz//Desktop");
        Assert.assertEquals(exp, act);
    }

}